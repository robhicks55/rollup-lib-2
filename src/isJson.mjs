/**
 * Determines if an string can be parsed into JSON
 * @param  {String} str String to check
 * @return {Boolean}    TRUE if can be parsed, otherwise false
 */
export function isJson(str) {
  try {
    if (JSON.parse(str)) return true;
  } catch(e) {
    return false;
  }
}
