/**
 * Determines if an object is an HTMLElement
 * @param  {Object}  o Object to check
 * @return {Boolean}   Whether the object is an HTMLElement
 */
export function isElement(o) {
  return typeof HTMLElement === "object" ? o instanceof HTMLElement : //DOM2
    o && typeof o === "object" && o !== null && o.nodeType === 1 && typeof o.nodeName==="string";
}
