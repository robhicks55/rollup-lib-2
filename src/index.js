export { isElement } from './isElement.mjs';
export { isFunction } from './isFunction.mjs';
export { isJson } from './isJson.mjs';
export { isNode } from './isNode.mjs';
export { isObject } from './isObject.mjs';
export { isSpecial } from './isSpecial.mjs';
