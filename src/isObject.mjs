/**
 * Determines of an item is an object (not an array)
 * @param  {[type]}  item Item to check
 * @return {Boolean}      TRUE if object otherwise false
 */
export function isObject(item) {
  return item && typeof item === 'object' && !Array.isArray(item);
}
