/**
 * Checks if candidate is a DOM node
 * @param candidate
 * @returns Boolean true if is a Node otherewise false
 */
export function isNode(candidate) {
  console.log(candidate)
  typeof Node === "object" ? candidate instanceof Node : candidate && typeof candidate === "object" && typeof candidate.nodeType === "number" && typeof candidate.nodeName === "string";
}
