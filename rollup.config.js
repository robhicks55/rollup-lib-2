const resolve = require('rollup-plugin-node-resolve');
const external = ['rollup-lib-3'];

export default [
  {
    external,
    input: 'src/index.js',
    plugins: [],
    output: {
      file: 'dist/rollup-lib-2.mjs',
      format: 'es'
    }
  },
  {
    input: 'src/index.js',
    plugins: [resolve()],
    output: {
      file: 'dist/rollup-lib-2.bundled.mjs',
      format: 'es'
    }
  }
];
