import { StringBuilder } from 'rollup-lib-3';

/**
 * Determines if an object is an HTMLElement
 * @param  {Object}  o Object to check
 * @return {Boolean}   Whether the object is an HTMLElement
 */
function isElement(o) {
  return typeof HTMLElement === "object" ? o instanceof HTMLElement : //DOM2
    o && typeof o === "object" && o !== null && o.nodeType === 1 && typeof o.nodeName==="string";
}

/**
 * Determines if an object is a function
 * @param  {object}  obj Object to test
 * @return {Boolean}     True if the object is a function. Otherwise false
 */
function isFunction(obj) {
  return typeof obj === 'function';
}

/**
 * Determines if an string can be parsed into JSON
 * @param  {String} str String to check
 * @return {Boolean}    TRUE if can be parsed, otherwise false
 */
function isJson(str) {
  try {
    if (JSON.parse(str)) return true;
  } catch(e) {
    return false;
  }
}

/**
 * Checks if candidate is a DOM node
 * @param candidate
 * @returns Boolean true if is a Node otherewise false
 */
function isNode(candidate) {
  console.log(candidate);
  typeof Node === "object" ? candidate instanceof Node : candidate && typeof candidate === "object" && typeof candidate.nodeType === "number" && typeof candidate.nodeName === "string";
}

/**
 * Determines of an item is an object (not an array)
 * @param  {[type]}  item Item to check
 * @return {Boolean}      TRUE if object otherwise false
 */
function isObject(item) {
  return item && typeof item === 'object' && !Array.isArray(item);
}

function isSpecial(str) {
  let sb = new StringBuilder(str);

  return sb.toString();
}

export { isElement, isFunction, isJson, isNode, isObject, isSpecial };
